import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
})
export class SigninComponent {
  // // All fields in this form are required
  authForm = new FormGroup({});

  constructor(private authService: AuthService) {}

  onSubmit() {
    // Return if the form is invalid
    // Call auth service sign up and if the response was OK navigate to '/inbox'
    // If the reponse was NOT OK render the show the user it was failed and reset inputs
  }
}
