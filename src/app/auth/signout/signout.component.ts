import { Component } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signout',
  templateUrl: './signout.component.html',
  styleUrls: ['./signout.component.css'],
})
export class SignoutComponent {
  constructor(private authService: AuthService) {}

  // Once the user visit this component, call sign out method of auth service and navigate the user to '/'
}
