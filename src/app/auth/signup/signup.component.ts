import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { MatchPassword } from '../validators/match-password';
import { UniqueUsername } from '../validators/unique-username';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent {
  // All fields in this form are required
  authForm = new FormGroup({}, {});

  constructor(
    private matchPassword: MatchPassword,
    private uniqueUsername: UniqueUsername,
    // use these two validations in your form
    private authService: AuthService
  ) {}

  onSubmit() {
    // Return if the form is invalid
    // Call auth service sign up and if the response was OK navigate to '/inbox'
    // If the reponse was NOT OK render the show the user it was failed and reset inputs
  }
}
